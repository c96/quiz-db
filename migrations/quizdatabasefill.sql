BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS `groups` (
	`group_id`	INTEGER NOT NULL,
	`group_name`	TEXT NOT NULL,
	PRIMARY KEY(`group_id`)
);
CREATE TABLE IF NOT EXISTS `users` (
	`user_id`	INTEGER NOT NULL,
	`group_id`	INTEGER NOT NULL,
	`username`	TEXT NOT NULL,
	`email`	TEXT NOT NULL,
	`password`	TEXT NOT NULL,
	FOREIGN KEY(`group_id`) REFERENCES `groups`(`group_id`),
	PRIMARY KEY(`user_id`)
);
CREATE TABLE IF NOT EXISTS `quizzes` (
	`quiz_id`	INTEGER NOT NULL,
	`group_id`	INTEGER NOT NULL,
	`quiz_name` TEXT NOT NULL,
	`start_time`	TEXT NOT NULL,
	`end_time`	TEXT NOT NULL,
	PRIMARY KEY(`quiz_id`),
	FOREIGN KEY(`group_id`) REFERENCES `groups`(`group_id`)
);
CREATE TABLE IF NOT EXISTS `questions` (
	`question_id`	INTEGER NOT NULL,
	`quiz_id`	INTEGER NOT NULL,
	`question_text`	TEXT NOT NULL,
	`correct_answer`	TEXT NOT NULL,
	PRIMARY KEY(`question_id`),
	FOREIGN KEY(`quiz_id`) REFERENCES `quizzes`(`quiz_id`)
);
CREATE TABLE IF NOT EXISTS `notifications` (
	`notification_id`	INTEGER NOT NULL,
	`group_id`	INTEGER NOT NULL,
	`notification_time`	TEXT NOT NULL,
	`notification_text`	TEXT NOT NULL,
	PRIMARY KEY(`notification_id`),
	FOREIGN KEY(`group_id`) REFERENCES `groups`(`group_id`)
);
CREATE TABLE IF NOT EXISTS `usersubmissions` (
	`submission_id`	INTEGER NOT NULL,
	`quiz_id`	INTEGER NOT NULL,
	`question_id`	INTEGER NOT NULL,
	`user_id`	INTEGER NOT NULL,
	`group_id`	INTEGER NOT NULL,
	`user_answer`	TEXT NOT NULL,
	`correct_answer`	TEXT NOT NULL,
	`submission_time`	TEXT NOT NULL,
	FOREIGN KEY(`group_id`) REFERENCES `groups`(`group_id`),
	FOREIGN KEY(`user_id`) REFERENCES `users`(`user_id`),
	FOREIGN KEY(`quiz_id`) REFERENCES `quizzes`(`quiz_id`),
	PRIMARY KEY(`submission_id`)
);
CREATE TABLE IF NOT EXISTS `classrooms` (
	`classroom_id`	INTEGER NOT NULL,
	`group_id`	INTEGER NOT NULL,
	`latitude`	REAL NOT NULL,
	`longitude`	REAL NOT NULL,
	`start_time`	TEXT NOT NULL,
	`end_time`	TEXT NOT NULL,
	FOREIGN KEY(`group_id`) REFERENCES `groups`(`group_id`),
	PRIMARY KEY(`classroom_id`)
);


/* 
CREATING INITIAL GROUPS 
*/

/* Groups 1 and 2 */
INSERT INTO `groups` VALUES (1,'Networks Group');
INSERT INTO `groups` VALUES (2,'Data Group');

/* 
CREATING INITIAL USERS 
*/

/* Users in Group 1 */
INSERT INTO `users` VALUES (1,1,'u1','u1@example.com','771E59F982C8D3AFF00CAF11E147007932368D53');
INSERT INTO `users` VALUES (2,1,'u2','u2@example.com','771E59F982C8D3AFF00CAF11E147007932368D53');
INSERT INTO `users` VALUES (3,1,'u3','u3@example.com','771E59F982C8D3AFF00CAF11E147007932368D53');
INSERT INTO `users` VALUES (4,1,'u4','u4@example.com','771E59F982C8D3AFF00CAF11E147007932368D53');

/* Users in Group 2 */
INSERT INTO `users` VALUES (5,2,'u5','u5@example.com','771E59F982C8D3AFF00CAF11E147007932368D53');
INSERT INTO `users` VALUES (6,2,'u6','u6@example.com','771E59F982C8D3AFF00CAF11E147007932368D53');
INSERT INTO `users` VALUES (7,2,'u7','u7@example.com','771E59F982C8D3AFF00CAF11E147007932368D53');
INSERT INTO `users` VALUES (8,2,'u8','u8@example.com','771E59F982C8D3AFF00CAF11E147007932368D53');

/* 
CREATING INITIAL QUIZZES 
*/

/* Quiz 1 and 2 for Group 1 */
INSERT INTO 'quizzes' VALUES (1,1,'network quiz 1','start placeholder','end placeholder');
INSERT INTO 'quizzes' VALUES (2,1,'network quiz 2','start placeholder','end placeholder');

/* Quiz 3 for Group 2 */
INSERT INTO 'quizzes' VALUES (3,2,'data quiz 1','start placeholder','end placeholder');

/* 
CREATING INITIAL QUESTIONS 
*/

/* Questions 1 and 2 for Quiz 1 */
INSERT INTO 'questions' VALUES (1,1,'What port is used for HTTP?','80');
INSERT INTO 'questions' VALUES (2,1,'What port is used for HTTPS?','443');

/* Question 3 for Quiz 2 */
INSERT INTO 'questions' VALUES (3,2,'What port is used for SSH?','22');

/* Question 4 for Quiz 3 */
INSERT INTO 'questions' VALUES (4,3,'Which SQL keyword(s) is used to sort the result-set?','ORDER BY');

/*
CREATING INITIAL LOCATIONS/CLASSROOMS
*/
INSERT INTO 'classrooms' VALUES (1,1,33.42,-111.94,'start placeholder','end placeholder');
INSERT INTO 'classrooms' VALUES (2,2,33.22,-111.24,'start placeholder','end placeholder');

COMMIT;
