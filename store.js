/* 
Knex.js is SQL query builder for SQLITE3 designed to be flexible, portable and fun to use
Document: http://knexjs.org/
*/
const knex = require('knex')(require('./knexfile'))

module.exports = {
    quizzesList () {
        console.log(`retrieving quizzes`);
        return knex('quizzes').select('*')
    },
    groupsList () {
        console.log(`retrieving quizzes`);
        return knex('groups').select('*')
    },
    usersList () {
        console.log(`retrieving quizzes`);
        return knex('users').select('*')
    }
}