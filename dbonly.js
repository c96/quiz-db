// Database only testing script

const sqlite3 = require('sqlite3').verbose();

let db = new sqlite3.Database('./database/quizzesdata.db', sqlite3.OPEN_READONLY, (err) => {
    if (err) {
        return console.error(err.message);
    }
    console.log('Connected to the in-memory SQlite database.');
});

// Get group

/*
db.serialize(() => {
console.log("Selecting quizzes from group 1");
    db.each(`select * from quizzes where group_id = 1`, (err, row) => {
        if (err) {
            console.error(err.message);
        }
        console.log(row.quiz_id + "\t" + row.quiz_name);
    });
});
*/

// Assuming user 1, group 1, get questions of all quizzes
db.serialize(() => {
    db.each(`select * from users where group_id = 1`, (err, row) => {
        if (err) {
            console.error(err.message);
        }
        console.log(row.user_id + "\t" + row.username+ "\t" + row.email);
    });
});

// Match user to group to quiz
db.serialize(() => {
    db.each(`select 
    quizzes.quiz_id, 
    quizzes.group_id, 
    users.user_id, 
    users.username 
    from quizzes 
    inner join users on users.group_id = quizzes.group_id 
    where quizzes.quiz_id = 3`, (err, row) => {
        if (err) {
            console.error(err.message);
        }
        console.log(row.quiz_id + "\t" + row.user_id);
    });
});

db.close((err) => {
    if (err) {
        return console.error(err.message);
    }
    console.log('Close the database connection.');
});